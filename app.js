class Card {
    constructor(post) {
      this.post = post;
      this.createCard();
    }

    createCard() {
        const cardContainer = document.querySelector('#container');
        const cardElement = document.createElement('div');
        cardElement.className = 'card';
        cardElement.dataset.postId = this.post.id;

        const cardImage = new Image();
        cardImage.className = 'image';
    
        const deleteButton = document.createElement('button');
        deleteButton.className = 'button'
        deleteButton.textContent = 'Delete';
        deleteButton.addEventListener('click', () => this.deleteCard());

        cardElement.innerHTML = `
        <h2>${this.post.title}</h2>
        <p>${this.post.body}</p>
        <p>Author: ${this.post.user.name} ${this.post.user.surname} (${this.post.user.email})</p>
      `;
      
      cardElement.appendChild(deleteButton);
      cardElement.appendChild(cardImage);
      cardContainer.appendChild(cardElement);
    
    }

    deleteCard() {
  
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
            method: 'DELETE'
        }).then(res => {
            if(res.ok){
                const cardElement = document.querySelector(`.card[data-post-id="${this.post.id}"]`);
                cardElement.remove();
            }else{
                console.error('Error deleting post:', res.status);
            }
        })
    }
}
  
fetch('https://ajax.test-danit.com/api/json/users')
.then(res => res.json())
.then(users => {
  fetch('https://ajax.test-danit.com/api/json/posts')
    .then(res => res.json())
    .then(posts => {
      posts.forEach(post => {
        const user = users.find(u => u.id === post.userId);
        post.user = user;
        new Card(post);
      });
    })
    .catch(error => console.error('Error fetching posts:', error));
})
.catch(error => console.error('Error fetching users:', error));